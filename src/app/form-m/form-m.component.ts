import { Component, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-form-m',
  templateUrl: './form-m.component.html',
  styleUrls: ['./form-m.component.css']
})
// @Injectable({providedIn:'root'})
export class FormMComponent implements OnInit {

  forumE!:FormGroup;
  constructor(private formBuilder: FormBuilder, private api: ApiService,
    private dialogRef: MatDialogRef<FormMComponent>) {}

  ngOnInit(): void {
    this.forumE = this.formBuilder.group({
      date: ['',Validators.required],
      description: ['',Validators.required]

    })

  }
  addData()
    {
      if(this.forumE.valid)
      {
        // this.api.postData(this.forumE.value).subscribe();
        // this.forumE.reset();
        this.dialogRef.close("Totooooo");

      // console.log(this.forumE.value);
      }

    }

}
