import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SecondTableFrameComponent } from "./second-table-frame/second-table-frame.component";
import { TableFrameComponent } from "./table-frame/table-frame.component";


const routes: Routes = [
    {path:'FirstTable', component: TableFrameComponent },
    {path:'SecondTable', component: SecondTableFrameComponent}
];

@NgModule({
    imports:[
        //pour enregistrer les routes racines de notre app
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})
export class AppRoutingModule
{

}
