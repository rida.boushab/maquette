import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstFrameComponent } from './first-frame.component';

describe('FirstFrameComponent', () => {
  let component: FirstFrameComponent;
  let fixture: ComponentFixture<FirstFrameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstFrameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
