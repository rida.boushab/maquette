import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { PopUpComponent } from '../pop-up/pop-up.component';

@Component({
  selector: 'app-first-frame',
  templateUrl: './first-frame.component.html',
  styleUrls: ['./first-frame.component.css']
})
export class FirstFrameComponent implements OnInit {

  constructor(private dialogPop: MatDialog){}
  openDialog()
  {
    this.dialogPop.open(PopUpComponent);
  }

  ngOnInit(): void {
  }

}
