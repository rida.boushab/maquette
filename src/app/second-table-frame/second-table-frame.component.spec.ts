import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondTableFrameComponent } from './second-table-frame.component';

describe('SecondTableFrameComponent', () => {
  let component: SecondTableFrameComponent;
  let fixture: ComponentFixture<SecondTableFrameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondTableFrameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondTableFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
