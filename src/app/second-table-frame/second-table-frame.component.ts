import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormMComponent } from '../form-m/form-m.component';
import { FormData } from '../model/form-data.model';
import { ApiService } from '../services/api.service';
import { MaquetteService } from '../services/maquette.service';

@Component({
  selector: 'app-second-table-frame',
  templateUrl: './second-table-frame.component.html',
  styleUrls: ['./second-table-frame.component.css']
})
export class SecondTableFrameComponent implements OnInit {

  headers;
  rows:any;
  @Output() updateDataEvent = new EventEmitter<any>();
  updateData()
  {
    this.updateDataEvent.emit('Je suis l\'enfant');
  }
  constructor( private dialog: MatDialog, private api:ApiService ) {
    this.headers = ["date",  "description"];

   }


  ngOnInit(): void
  {
     this.api.getData().subscribe(data => {
       this.rows = data;

     });
  }
  openDialog()
  {
    this.dialog.open(FormMComponent,
      {
        width: '70%'
      }).afterClosed().subscribe(data => {
        console.log(data);
      });
  }
  getFdata()
  {
    this.api.getData().subscribe();
  }

  deletRow(id: number)
  {
    this.api.deletData(id).subscribe({
      next:(res) => {
        alert("Les données sont supprimées");
        location.reload();
      }
    });
  }

}
