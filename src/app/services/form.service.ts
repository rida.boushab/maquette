import { Injectable } from "@angular/core";
import { Data } from "../model/data.model";

@Injectable({providedIn:'root'})
export class MaquetteService{
    columns: Data[] = [
        {
            "DATE" : "18/09/2018",
            "TYPE" : "Prélévement",
            "CONTRAT" : "Prévoyance",
            "DESCRIPTION" : "Cotisation mensuelle",
            "MONTANT" : "5,52"
        },
        {
            "DATE" : "18/09/2018",
            "TYPE" : "Prélévement",
            "CONTRAT" : "Prévoyance",
            "DESCRIPTION" : "Cotisation mensuelle",
            "MONTANT" : "5,52"
        },
        {
            "DATE" : "18/09/2018",
            "TYPE" : "Prélévement",
            "CONTRAT" : "Prévoyance",
            "DESCRIPTION" : "Cotisation mensuelle",
            "MONTANT" : "5,52"
        }
    ];
    getData(): Data[]{
        console.log(this.columns);

        return this.columns;
    }
}
