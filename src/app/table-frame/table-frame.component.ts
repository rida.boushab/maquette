import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Data } from '../model/data.model';
import { PopUpComponent } from '../pop-up/pop-up.component';
import { MaquetteService } from '../services/maquette.service';

@Component({
  selector: 'app-table-frame',
  templateUrl: './table-frame.component.html',
  styleUrls: ['./table-frame.component.css']
})
export class TableFrameComponent implements OnInit {
  rows!: Data[];
  headers;
  @Input() vari: any;
  constructor(private dataService:MaquetteService, private dialogPop: MatDialog ) {
    this.rows = this.dataService.getData();
    this.headers = ["DATE", "TYPE", "CONTRAT", "DESCRIPTION", "MONTANT"];
   }

  ngOnInit(): void {
    // this.rows = this.dataService.getData();
    // console.log(this.rows.DATE);

  }
  openDialog()
  {
    this.dialogPop.open(PopUpComponent);
  }
  getRow(row:Data, col:string)
  {
    for (const [key, value] of Object.entries(row)) {
      if(key === col)
      {
        return value;
      }

      // console.log(`${key}: ${value}`);
    }
    return "";
  }

}
